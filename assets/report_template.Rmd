---
title: "Results of `r title` workflow run"
date: '`r format(Sys.Date(), "%B %d, %Y")`'
output: html_document

params:
  command: ""
  resultdir: ""
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(python.reticulate = FALSE)
library(ggplot2)
library(ggpubr)
```

# Reads  and gaps
```{r upload_table_length, fig.align="center", warning = FALSE}
reads_length_table <- read.csv("reads_length.csv", header = FALSE, sep=";")
reads_length_table$tool <- "reads"

real_length <- read.csv("real_length.csv", header = TRUE, sep=";")
x_max <- max(reads_length_table$V2, real_length$length)/1000
x_min <- min(reads_length_table$V2, real_length$length)/1000
total_gaps  <- nrow(real_length)
total_reads <- nrow(reads_length_table)
total_reads_size <- sum(reads_length_table$V2)

reads_info <- paste('Number of reads:', total_reads, "\nTotal reads length:",total_reads_size, sep=" ")
gap_info   <- paste('Number of gaps:', total_gaps, sep=" ")

#summary(filled_table)
plot1 <- ggplot() +
  geom_histogram(data=reads_length_table, aes(x = V2/1000), binwidth = 5) +
  xlim(x_min, x_max) + labs(x = "Length (in kb)", y = "Count", title = "Count of reads per length") +
  theme_bw()
plot2 <- ggplot() +
  geom_histogram(data=real_length, aes(x = length/1000), binwidth = 5) +
  xlim(x_min, x_max) + labs(x = "Length (in kb)", y = "Count", title = "Count of gaps per length") +
  theme_bw()

y1_max <- max(ggplot_build(plot1)$data[[1]]$count)
y2_max <- max(ggplot_build(plot2)$data[[1]]$count)

plots <- ggarrange(plot1 + geom_text(aes(x=x_max-50, y=y1_max, label=reads_info)), plot2 + geom_text(aes(x=x_max-50, y=y2_max, label=gap_info)), widths = c(3,3))
plots
real_length <- real_length[which(real_length$length>0),]
```

```{r save_plot1, include=FALSE}
png('plot1.png')
plot(plots)
dev.off()
```

# State of gaps after gapclosing
```{r count_gap_type, fig.align="center", warning=FALSE}
filled_table <- read.csv("gap_length.csv", header = TRUE, sep=";")
frequency_table <- as.data.frame(table(filled_table$tool, filled_table$gap_type))
frequency_table[frequency_table==0] <- NA

plots <- ggplot(data=frequency_table, aes(x = Var1, y = Freq, fill = Var2, label = Freq)) +
  geom_bar(stat="identity") + 
  geom_text(size = 3, position = position_stack(vjust = 0.5)) +
  #scale_x_discrete(limits=c("TGS", "LR_kgl", "LR_ugl")) +
  labs(x = "Tool", y = "Count", title = "Number of gaps (partially) filled or unchanged") +
  scale_fill_discrete(name = "Gap type", labels = c("Filled", "Unchanged", "Partially filled")) +
  theme_bw()
plots
```

```{r save_plot2, include=FALSE}
png('plot2.png')
plot(plots)
dev.off()
```

# Size of filled gaps
```{r compare_length, fig.align="center", warning=FALSE}
merged_gaps <- filled_table[which(filled_table$gap_type=="F"),c("tool", "length", "gap_name")]
merged_gaps <- merge(merged_gaps, real_length, by = "gap_name")

plots <- ggplot(data=merged_gaps, aes(x=length.x, y=length.y, group=tool.x)) +
  geom_point(aes(color=tool.x)) + geom_abline(intercept=0, slope=1) + 
  labs(x = "Real length", y = "Length filled gaps", title = "Length of filled gaps vs. real length of these gaps", color="Gap type")+
  scale_x_continuous(expand = c(0, 0), limits = c(0,31000)) + 
  scale_y_continuous(expand = c(0, 0), limits = c(0, 31000)) +
  theme_bw()
plots
```

```{r save_plot3, include=FALSE}
png('plot3.png')
plot(plots)
dev.off()
```

```{r length_realVSfilled, fig.align="center", warning=FALSE}
ggplot(data=merged_gaps, aes(x=tool.x, y=length.y - length.x)) +
  geom_boxplot() +
  #scale_x_discrete(limits=c("TGS", "LR_kgl", "LR_ugl")) +
  labs(x = "Tool", y = "Log10 of length difference", title = "Log10 of difference of length between filled gap and known length") +
  scale_y_log10() + theme_bw()
```

```{r strength_length, fig.align="center", warning=FALSE}
mean_test_table <- FALSE

for (tool in unique(merged_gaps$tool.x)) {
  extracted_table <- merged_gaps[which( merged_gaps$tool.x==tool ),]
  shapiro_test <- shapiro.test(extracted_table$length.x - extracted_table$length.y)$p.value
  
  if (shapiro_test < 0.05) {
    mean_test_pvalue <- t.test(extracted_table$length.x, extracted_table$length.y, paired=TRUE)$p.value
    mean_test <- "student"
  } else {
    mean_test_pvalue <- wilcox.test(extracted_table$length.x, extracted_table$length.y, paired=TRUE)$p.value
    mean_test <- "wilcoxon"
  }
  
  if (class(mean_test_table)=="logical") {
    mean_test_table <- data.frame(tool, shapiro_test, mean_test, mean_test_pvalue)
  } else {
    new_row <- matrix(c(tool, shapiro_test, mean_test, mean_test_pvalue), nrow = 1, ncol = 4)
    colnames(new_row) <- colnames(mean_test_table)
    mean_test_table <- rbind(mean_test_table, new_row)
  }
}

knitr::kable(mean_test_table)
```

# Identity of filled gaps vs expected sequence
```{r compare_identity, fig.align="center", warning=FALSE}
id_table <- read.csv("gap_id.csv", header = TRUE, sep=";")

for (tool in unique(id_table$tool)) {
  selected_tool <- id_table[which(id_table$tool == tool),]
  selected_gaps <- filled_table[which(merged_gaps$tool.x == tool),]
  
  if (nrow(selected_tool)<nrow(selected_gaps)) {
    new_lines <- matrix(c("unknown", tool, 0), nrow = nrow(selected_gaps) - nrow(selected_tool), ncol = 3, byrow = TRUE)
    colnames(new_lines) <- colnames(id_table)
    id_table <- rbind(id_table, new_lines)
  }
}
id_table$id_percent <- as.numeric(as.character(id_table$id_percent))

plots <- ggplot(data=id_table, aes(x=tool, y=id_percent)) +
  #scale_x_discrete(limits=c("TGS", "LR_kgl", "LR_ugl")) +
  labs(x = "Tool", y = "identity (in %)", title = "Percentage of identity between filled gaps and known sequences") +
  geom_boxplot() + theme_bw()
plots
```

```{r save_plot4, include=FALSE}
png('plot4.png')
plot(plots)
dev.off()
```

```{r strength_identity, warning=FALSE}
id_test_table <- FALSE

for (tool in unique(id_table$tool)) {
  extracted_table <- id_table[which( id_table$tool==tool ),]
  shapiro_test <- shapiro.test(extracted_table$id_percent)$p.value
  
  if (shapiro_test < 0.05) {
    mean_test_pvalue <- t.test(extracted_table$id_percent, mu=100)$p.value
    mean_test <- "student"
  } else {
    mean_test_pvalue <- wilcox.test(extracted_table$id_percent, mu=100)$p.value
    mean_test <- "wilcoxon"
  }
  
  if (is.logical(id_test_table)) {
    id_test_table <- data.frame(tool, shapiro_test, mean_test, mean_test_pvalue)
  } else {
    new_row <- matrix(c(tool, shapiro_test, mean_test, mean_test_pvalue), nrow = 1, ncol = 4)
    colnames(new_row) <- colnames(id_test_table)
    id_test_table <- rbind(id_test_table, new_row)
  }
}

knitr::kable(id_test_table)
```

# Résumé
```{r gap_summary, fig.align="center"}
levels(frequency_table$Var2) <- c(levels(frequency_table$Var2), "B")
index = 1

for (tool in unique(frequency_table$Var1)) {
  frequency_table <- rbind(frequency_table, c(tool, 'B', NA))
  id_tool <- id_table[which(id_table$tool == tool),]
  
  nb_bad <- nrow(id_tool[which(id_tool$id_percent<90),])
  if (!is.null(nb_bad)) {
    frequency_table[which((frequency_table$Var1==tool) & (frequency_table$Var2=="B")), "Freq"] <- nb_bad
    nb_filled = frequency_table[which((frequency_table$Var1==tool) & (frequency_table$Var2 == "F")), "Freq"]
    frequency_table[which((frequency_table$Var1==tool) & (frequency_table$Var2 == "F")), "Freq"] <- as.numeric(nb_filled) - nb_bad
  }
}
frequency_table[frequency_table==0] <- NA

plots <- ggplot(data=frequency_table, aes(x = Var1, y = as.numeric(Freq), fill = Var2, label = Freq)) +
  geom_bar(stat="identity") + 
  geom_text(size = 3, position = position_stack(vjust = 0.5)) +
  #scale_x_discrete(limits=c("TGS", "LR_kgl", "LR_ugl")) +
  labs(x = "Tool", y = "Count", title = "Number of gaps (partially) filled, joined or unchanged") +
  scale_fill_discrete(name = "Gap type", labels = c("Filled", "Unchanged", "Partially filled", "Badly filled")) +
  theme_bw()
plots
```

```{r save_plot5, include=FALSE}
png('plot5.png')
plot(plots)
dev.off()
```

# Memory and running time
```{r efficiency}
efficiency_table <- read.table("pipeline_trace.txt", header = TRUE, sep = "\t")
efficiency_table <- efficiency_table[which(efficiency_table$process == "gapcloser"),]
efficiency_table$rss <- efficiency_table$rss/2^20
```
```{r mem_vs_time}
sp <- ggplot(efficiency_table, aes(realtime, rss, label = tag))+
  geom_point() + geom_text(hjust=0, vjust=-1)
sp
```

```{r save_plot6, include=FALSE}
png('plot6.png')
plot(sp)
dev.off()
```

# Settings
Command line used to run the nextflow pipeline: 

`r params$command`

Results are stored in: 

`r params$resultdir`