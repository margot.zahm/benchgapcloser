#!/usr/bin/env python3

import argparse
from Bio import SeqIO
from numpy import random


parser = argparse.ArgumentParser(description='Generate a fake scaffold eg. a unique sequence composed of gaps and contigs')
parser.add_argument('--contig_size', nargs=2, type=int, default=[300000, 50000], help="Mean and standard deviation of contig length (default: 300000 50000).")
parser.add_argument('--gap_size', nargs=2, type=int, default=[20000, 5000], help="Mean and standard deviation of gap length (default: 20000 5000).")
parser.add_argument('--seq_size', type=int, default=30000000, help="Total size of scaffold in bp (default: 30000000).")
parser.add_argument('--assembly', default=False, help="Assembly file in fasta format from which gaps are generated")
parser.add_argument('--seed', type=int, default=0, help="One seed always generate the same random numbers.")
parser.add_argument('-o', '--output', type=str, default="random_seq", help="output prefix")

args=parser.parse_args()


c_mean, c_stdev = args.contig_size
g_mean, g_stdev = args.gap_size
seq_size = args.seq_size
ouput_p = args.output

if args.seed:
	random.seed(args.seed)

DNA = ["A", "T", "C", "G"]


def generate_random_seq(length):
	seq = ''.join(random.choice(DNA, length))

	return seq


def return_random(median, stdev):
	return int(random.normal(median, stdev))


def add_gaps(sequence):
	i              = 1
	start          = 0
	contig_seq     = []
	gap_seq        = []
	gap_coord      = []
	new_seq_gapkl  = ''
	new_seq_gapukl = ''

	while start < len(sequence):
		contig_size = return_random(c_mean, c_stdev)
		gap_size = return_random(g_mean, g_stdev)

		if start + contig_size + gap_size > len(sequence):
			contig_seq.append(sequence[start:])
			new_seq_gapukl += sequence[start:]
			new_seq_gapkl  += sequence[start:]

		else:
			contig_seq.append(sequence[start:start+contig_size])
			new_seq_gapukl += sequence[start:start+contig_size]
			new_seq_gapukl += 'N' * 500
			new_seq_gapkl  += sequence[start:start+contig_size]
			new_seq_gapkl  += 'N' * gap_size if gap_size > 0 else 'N' * 500

			if gap_size > 0:
				gap_seq.append(sequence[start + contig_size : start + contig_size + gap_size])
			else:
				gap_seq.append(sequence[start + contig_size + gap_size : start + contig_size])
			gap_coord.append(f"{ouput_p}\t{start + contig_size + 1}\t{start + contig_size + gap_size}\tgap_{i}\n")

		start = start + contig_size + gap_size
		i += 1

	return contig_seq, gap_seq, gap_coord, new_seq_gapkl, new_seq_gapukl

if args.assembly:
	assembly = args.assembly
	for record in SeqIO.parse(assembly, "fasta"):
		seq = record.seq
else:
	seq = generate_random_seq(seq_size)
	random_seq_file = open(f"{ouput_p}.fa", 'w')
	random_seq_file.write(f">{ouput_p}\n{seq}\n")
	random_seq_file.close()

contig_seq, gap_seq, gap_coord, new_seq_gapkl, new_seq_gapukl = add_gaps(seq)

gap_seq_file           = open(f"{ouput_p}_gap_sequences.fa", 'w')
gap_coord_file         = open(f"{ouput_p}_gap_coord.bed", 'w')
contig_seq_file        = open(f"{ouput_p}_contigs.fa", "w")
random_seq_gapkl_file  = open(f"{ouput_p}_gapknown.fa", 'w')
random_seq_gapukl_file = open(f"{ouput_p}_gapunknown.fa", 'w')

[gap_seq_file.write(f">gap_{i+1}\n{seq}\n") for i, seq in enumerate(gap_seq)]
[gap_coord_file.write(line) for line in gap_coord]
[contig_seq_file.write(f">contig_{i}\n{seq}\n") for i, seq in enumerate(contig_seq)]
random_seq_gapkl_file.write(f">{ouput_p}\n{new_seq_gapkl}\n")
random_seq_gapukl_file.write(f">{ouput_p}\n{new_seq_gapukl}\n")

gap_seq_file.close()
gap_coord_file.close()
contig_seq_file.close()
random_seq_gapkl_file.close()
random_seq_gapukl_file.close()
