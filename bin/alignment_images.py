#!/usr/bin/env python3

import argparse
import genomeview

parser = argparse.ArgumentParser(description='Generates an image in SVG format of alignment like IGV.')
parser.add_argument('--bam', type=str, nargs='+', help="Alignment file(s) in bam format. Must be indexed.")
parser.add_argument('--ref', type=str, help="Reference file on which alignment file(s) were generated.")
parser.add_argument('--chrom', type=str, help="Chromosome on which the image is taken.")
parser.add_argument('--start', type=int, help="Start coordinate of the image.")
parser.add_argument('--end', type=int, help="End coordinate of the image.")
parser.add_argument('--output', type=str, help="Output file name.")
args=parser.parse_args()

# dataset_paths = args.bam
# reference = args.ref

# chrom = args.chrom
# start = args.start
# end   = args.end

doc = genomeview.visualize_data(args.bam, args.chrom, args.start, args.end, args.ref)
