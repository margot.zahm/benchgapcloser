#!/usr/bin/env python3

import sys
from Bio import SeqIO

records = list(SeqIO.parse(sys.argv[1], "fasta"))
i = 1

for record in records:
	seq_len = len(record.seq)
	nb_N    = record.seq.count("N")

	if nb_N == seq_len:
		print(f"{seq_len};N;gap_{i}")
	elif nb_N > 0:
		print(f"{seq_len};P;gap_{i}")
	else:
		print(f"{seq_len};F;gap_{i}")

	i+=1