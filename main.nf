ContigLength    = Channel.value(params.contig_length)
GapLength       = Channel.value(params.gap_length)
ScaffLength     = Channel.value(params.scaffold_length)
Seed            = Channel.value(params.seed)
ReportTemplate  = Channel.fromPath(params.report_template)
TraceFile       = Channel.fromPath("${params.outdir}/pipeline_trace.txt")

BadReadQuantity   = Channel.value(params.quantity)
BadReadErrorMod   = Channel.value(params.error_model)
BadReadQscoreMod  = Channel.value(params.qscore_model)
BadReadGlitches   = Channel.value(params.glitches)
BadReadJunk       = Channel.value(params.junk_reads)
BadReadRandom     = Channel.value(params.random_reads)
BadReadChimeras   = Channel.value(params.chimeras)
BadReadIdentity   = Channel.value(params.identity)
BadReadAdapterS   = Channel.value(params.start_adapter_seq)
BadReadAdapterE   = Channel.value(params.end_adapter_seq)
BadReadLength     = Channel.value(params.length)

Assembly  = file(params.assembly)

def gapcloser_soft = []
if (!params.all_gapcloser && !params.GM_gapcloser && !params.LR_gapcloser && !params.TGS_gapcloser) {
	exit 1, "You must specify at least one gapcloser to use or all gapclosers with parameter --all_gapcloser"
}
if (params.GM_gapcloser) {
	gapcloser_soft.add('GM')
}
if (params.LR_gapcloser) {
	gapcloser_soft << 'LR'
}
if (params.TGS_gapcloser) {
	gapcloser_soft.add('TGS')
}


/**
STEP 1 : GENERATE DATA
**/

process generate_random_fasta {
	publishDir "${params.outdir}/data/", pattern:"random_seq.fa"
	publishDir "${params.outdir}/data/", pattern:"random_seq_gap_coord.bed"

	input:
	file assembly from Assembly
	val contig_length from ContigLength
	val gap_length from GapLength
	val scaffold_length from ScaffLength
	val seed from Seed

	output:
	file 'random_seq.fa' into CompleteAssembly
	tuple val('kgl'), file('random_seq_gapknown.fa') into KnownGap
	tuple val('ugl'), file('random_seq_gapunknown.fa') into UnknownGap
	file 'random_seq_contigs.fa' into SepContigs
	file 'random_seq_gap_sequences.fa' into GapSeq
	file 'random_seq_gap_coord.bed' into GapCoord4Length, GapCoord4Intersect

	script:
	def sequence = assembly.name != 'NO_FILE' ? "--assembly $assembly; mv $assembly random_seq.fa" : ''
	"""
	generate_data.py --contig_size ${contig_length} --gap_size ${gap_length} --seq_size ${scaffold_length} --seed ${seed} ${sequence}
	"""
}

if (!params.reads && !params.reads_coord) {
	process badread_simulation {
		publishDir "${params.outdir}/data/"
		label 'cluster'

		input:
		file assembly from CompleteAssembly
		val quantity from BadReadQuantity
		val error_model from BadReadErrorMod
		val qscore_model from BadReadQscoreMod
		val glitches from BadReadGlitches
		val junk_reads from BadReadJunk
		val random_reads from BadReadRandom
		val chimeras from BadReadChimeras
		val identity from BadReadIdentity
		val start_adapter from BadReadAdapterS
		val end_adapter from BadReadAdapterE
		val length from BadReadLength
		val scaffold_length from ScaffLength
		val seed from Seed

		output:
		file 'reads.fq' into FastqReads
		file 'reads_pos.bed' into ReadsPos

		script:
		def seed_param = seed != 0 ? "--seed $seed" : ''
		"""
		badread simulate --reference ${assembly} --quantity ${quantity} --error_model ${error_model} \\
			--qscore_model ${qscore_model} --glitches ${glitches} --junk_reads ${junk_reads} --chimeras ${chimeras} \\
			--random_reads ${random_reads} --identity ${identity} --start_adapter_seq "${start_adapter}" \\
			--end_adapter_seq "${end_adapter}" --length ${length} ${seed_param} > reads.fq

		grep ^@ reads.fq | sed -e 's/,/ /g' -e 's/ /\\t/g' -e 's/@//' | cut -f-4 |sed 's/\\(.*\\)-/\\1\\t/' |\\
		 awk '\$3=="+strand" {print \$2"\\t"\$4"\\t"\$5"\\t"\$1}
		 (\$3=="-strand" && ${scaffold_length}-\$5>=0) {print \$2"\\t"${scaffold_length}-\$5"\\t"${scaffold_length}-\$4"\\t"\$1}'\\
		 > reads_pos.bed
		"""
	}
} else if (params.reads && params.reads_coord) {
	FastqReads = Channel.fromPath(params.reads)
	ReadsPos   = Channel.fromPath(params.reads_coord)
} else {
	error "If you want to use an existing reads file, you must specify a read file with --reads and their coordinates\n on the assembly in bed format with --reads_coord"
}

process samtools_index {
	input:
	file reads from FastqReads

	output:
	tuple file("${reads}"), file("${reads}.fai") into ReadsIndexed
	file("${reads}.fai") into ReadsIndex

	script:
	"""
	samtools faidx ${reads}
	"""
}

process reads_length {
	publishDir "${params.outdir}/data/"

	input:
	file index from ReadsIndex

	output:
	file "reads_length.csv" into ReportReads

	script:
	"""
	awk '{print \$1";"\$2}' ${index} > reads_length.csv
	"""
}

process select_reads_list {
	label 'cluster'

	input:
	file reads from ReadsPos
	file gaps from GapCoord4Intersect

	output:
	file "reads_to_keep.txt" into ReadsList

	script:
	if( params.mode == 'overlap' )
		"""
		bedtools intersect -F 1 -a ${reads} -b ${gaps} | cut -f4 > reads_to_keep.txt
		bedtools intersect -v -a ${reads} -b ${gaps} | cut -f4 >> reads_to_keep.txt
		"""
	else if( params.mode == 'no_overlap' )
		"""
		cut -f4 ${reads} > reads_list.txt
		bedtools intersect -F 1 -a ${reads} -b ${gaps} | cut -f4 > reads_to_remove.txt

		comm 23 <(sort -S3G reads_list.txt) <(sort -S3G reads_to_remove.txt) > reads_to_keep.txt
		"""
	else if ( params.mode == 'all' )
		"""
		cut -f4 ${reads}  > reads_to_keep.txt
		"""
	else
		error "Invalid alignment mode: ${mode}. Must be one of these: 'no_overlap', 'overlap' or 'all' (default value)"

}

process fastq2fasta {
	input:
	tuple file(reads), file(index) from ReadsIndexed
	file filter from ReadsList

	output:
	file 'reads.fa' into FastaReads, ReadsAlign

	script:
	"""
	samtools faidx -r ${filter} ${reads} > reads.fa
	"""
}

/**
STEP 2 : RUN OF GAP CLOSING TOOLS
**/

GapcloserAssembly = KnownGap.concat(UnknownGap)
GapCloserData = GapcloserAssembly.spread(FastaReads)

process gapcloser {
	label 'cluster'
	tag "$software$gap_type"
	publishDir "${params.outdir}/data/", pattern:"*_assembly.fa"

	input:
	tuple val(gap_type), file(assembly), file(reads) from GapCloserData
	each software from gapcloser_soft

	output:
	tuple val("${software}${gap_type}"), file("${software}${gap_type}_assembly.fa") into GapClosedAssembly

	script:
	if (software=='GM') {
        """
        gmcloser -t ${assembly} -q ${reads} -p gapclosing -n ${task.cpus} -lr

    	last_iteration=\$(for dir in iteration-*; do echo \$dir; done | tail -1)
    	mv \$last_iteration/gapclosing.gapclosed.fa ${software}${gap_type}_assembly.fa
        """
    } else if (software=='LR') {
        """
		echo $PATH
        LR_Gapcloser.sh -i ${assembly} -l ${reads} -o ${gap_type}_output -s n -t ${task.cpus} -r 3

    	last_iteration=\$(for LRdir in ${gap_type}_output/iteration-*; do echo \$LRdir; done | tail -1)
    	mv \$last_iteration/gapclosed.fasta ${software}${gap_type}_assembly.fa
        """
    } else if (software=='TGS') {
        """
        tgsgapcloser \
		--scaff ${assembly} \
		--reads ${reads} \
		--output ${software}${gap_type} \
		--ne \
		--thread $task.cpus \

		mv ${software}${gap_type}.scaff_seqs ${software}${gap_type}_assembly.fa
        """
    }
}

process extract_gaps {
	label 'cluster'

	input:
	tuple val(gap_type), file(assembly) from GapClosedAssembly
	file contigs from SepContigs

	output:
	tuple val("${gap_type}"), file("${gap_type}_gaps_filled.fa") into FilledGapSeq, LengthGapSeq
	tuple val("${gap_type}"), file("${gap_type}_contig_map.bam"), file("${gap_type}_contig_map.bam.bai") into Contigs
	tuple val("${gap_type}"), file("${assembly}") into Assembly4Align
	tuple val("${gap_type}"), file("${gap_type}_gaps_coord.txt") into GapsCoord

	shell:
	'''
	minimap2 -a !{assembly} !{contigs} | samtools view -bh | samtools sort -o !{gap_type}_contig_map.bam
	samtools index !{gap_type}_contig_map.bam
	
	samtools faidx !{assembly}
	awk '{print $1"\\t1\\t"$2}' !{assembly}.fai > !{assembly}.bed

	bedtools coverage -d -a !{assembly}.bed -b !{gap_type}_contig_map.bam > !{gap_type}_coverage.bed
	awk 'BEGIN {start=0; stop=0} 
		($5==0 && $4>stop+1) {print $1":"start+1"-"stop+1; start=$4; stop=$4} 
		($5==0 && $4==stop+1) {stop=$4} 
		END {print $1":"start+1"-"stop+1}' !{gap_type}_coverage.bed | sed '1d' > !{gap_type}_gaps_coord.txt

	samtools faidx -r !{gap_type}_gaps_coord.txt !{assembly} > !{gap_type}_gaps_filled.fa
	'''
}

/**
STEP 3 : COMPARE FILLED GAP LENGTH WITH KNOWN LENGTH
**/

process real_gaps_length {
	publishDir "${params.outdir}/data/"

	input:
	file bed from GapCoord4Length

	output:
	file "real_length.csv" into ReportReal

	shell:
	'''
	echo -e "tool;length;gap_name" > real_length.csv
	awk '{print "real;"$3-$2";"$4}' !{bed} >> real_length.csv
	'''
}

process gaps_filled_length {	
	input:
	tuple val(tool), file(gap_seq) from LengthGapSeq

	output:
	file("${tool}_gap_length.csv") into LengthFiles
	tuple val(tool), file("${tool}_gap_length.csv") into SelectFilledGaps

	script:
	"""
	generate_lengthfile.py ${gap_seq} |sed 's/^/${tool};/' > ${tool}_gap_length.csv
	"""
}

process merge_gaps_length {
	publishDir "${params.outdir}/data/"

	input:
	file length_files from LengthFiles.collect()

	output:
	file "gap_length.csv" into ReportLength

	script:
	"""
	echo -e "tool;length;gap_type;gap_name" > gap_length.csv
	cat ${length_files} >> gap_length.csv
	"""
}

/**
STEP 4 : COMPARE FILLED GAP SEQUENCE WITH KNOWN SEQUENCE
**/

SelectGapSeq = FilledGapSeq.join(SelectFilledGaps)
process generate_id_table {
	input:
	tuple val(tool), file(gap_seq), file(gaps_filled) from SelectGapSeq
	file ref_seq from GapSeq

	output:
	file "${tool}_identity.csv" into IndentityTables

	shell:
	'''
	sed '1d' !{gaps_filled} | awk 'BEGIN {FS=";"} $3=="F" {print $4}' > gaps_filled.txt

	blat !{ref_seq} !{gap_seq} !{tool}_gap_map.psl
	cut -f1-17 !{tool}_gap_map.psl | grep -wf gaps_filled.txt | awk '$11>=$15 {size=$11} $11<$15 {size=$15} {print $14";!{tool};"$1/size*100}' > !{tool}_identity.csv
	'''
}

process merge_id_tables {
	publishDir "${params.outdir}/data/"

	input:
	file id_tables from IndentityTables.collect()

	output:
	file "gap_id.csv" into ReportId

	script:
	"""
	echo -e "gap_name;tool;id_percent" > gap_id.csv
	cat ${id_tables} |sed 's/random_seq0_gap/gap_/' >> gap_id.csv
	"""
}

/**
STEP 5 : GENERATE REPORTS AND IMAGES
**/

process align_reads {
	label 'cluster'

	input:
	tuple val(gap_type), file(assembly) from Assembly4Align
	file reads from ReadsAlign.collect()

	output:
	tuple val("${gap_type}"), file("${gap_type}_reads.bam"), file("${gap_type}_reads.bam.bai") into Reads4Image
	tuple val("${gap_type}"), file("${assembly}") into Assembly4Image

	script:
	"""
	minimap2 -t ${task.cpus} -a ${assembly} ${reads} | samtools view -@ ${task.cpus} -bh | samtools sort -@ ${task.cpus} -o ${gap_type}_reads.bam
	samtools index -@ ${task.cpus} ${gap_type}_reads.bam
	"""
}

GapsCoord4Image = GapsCoord.splitText().map{ [it[0], it[1].split("\n")[0]] }
GapsAssembly4Image = GapsCoord4Image.combine(Assembly4Image, by:0)
GapsAssemblyContig4Image = GapsAssembly4Image.combine(Contigs, by:0)
GapsAssemblyContigReads4Image = GapsAssemblyContig4Image.combine(Reads4Image, by:0)

process generate_alignment_images {
	label 'cluster'
	publishDir "${params.outdir}/images/${gap_type}/"

	input:
	tuple val(gap_type), val(coord), file(ref), file(contig_bam), file(contig_index), file(reads_bam), file(reads_index) from GapsAssemblyContigReads4Image

	output:
	file "*.svg"

	shell:
	'''
	chr=$(echo !{coord} | cut -d ':' -f1)
	start=$(($(echo !{coord} | cut -d ':' -f2 | cut -d '-' -f1)-1000))
	end=$(($(echo !{coord} | cut -d ':' -f2 | cut -d '-' -f2)+1000))

	alignment_images.py --bam !{contig_bam} !{reads_bam} --ref !{ref} --chr $chr --start $start --end $end --output $chr.$start-$end.svg
	'''
}

process generate_report {
	publishDir "${params.outdir}/", pattern:"report.html"
	publishDir "${params.outdir}/plots/", pattern:"plot*.png"
	stageInMode "copy"

	input:
	file report from ReportTemplate
	file length from ReportLength
	file real from ReportReal
	file reads from ReportReads
	file id from ReportId
	file trace from TraceFile

	output:
	file 'report.html'
	file "plot*.png"

	script:
	"""
	R -e "title='$workflow.manifest.name'; rmarkdown::render('${report}',output_file='report.html', params = list(command = '$workflow.commandLine', resultdir = '$workflow.launchDir'))"
	"""
}